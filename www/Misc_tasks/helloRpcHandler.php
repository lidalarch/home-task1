<?php

class Request {

    protected $data = [];

    public function __construct(array $data) {
        $this->data = $data;
    }

    public function keyExists($key): bool {
        return array_key_exists($key, $this->data);
    }

    public function get($key) {
        return $this->keyExists($key) ? $this->data[$key] : null;
    }

}

class HelloRpcHandler {

    public function handle(Request $request) {
        if (!$this->isRequestValid($request)) {
            throw new Exception('Request is invalid');
        }

        return json_encode([
            sprintf('Hello, %s', $request->get('username'))
        ]);
    }

    protected function isRequestValid(Request $request): bool {
        return $request->keyExists('username');
    }

}
