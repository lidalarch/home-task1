<!DOCTYPE html>
<html lang="ru">
<html>
    <head>
        <meta charset="UTF-8">
        <title>Перебор фруктов</title>
    </head>
    <body>
        <?php
        
        $fruits = [
            '1' => 'яблоко',
            '2' => 'груша',
            '3' => 'банан',
            '4' => 'слива',
            '5' => 'абрикос'
            ];
        
        echo '1)<br>';
        
        foreach ($fruits as $i) {
            echo $i.' ';
        }
        
        echo '<br>';
        
        $col = count($fruits);
        
        for ($i=1; $i<=$col; $i+=2) {
            echo $fruits[$i].' ';
        }
        
        echo '<br>';
        
        $i = 1;
        while ($i <= $col) {
            echo $fruits[$i].' ';
            $i++;
        }
        
        echo '<br>2)<br>';
        
        $arrMap = [];
        
        $f = function ($a) {
           echo "фрукт $a, ";
           return;
        };
        
        $arrMap = array_map($f, $fruits);
        
        echo '<br>';

        $arrWalk = array_walk($fruits, function (&$a) {
                                        $a .= ' - это фрукт';
                                        } );
        print_r (['$fruits' => $fruits]);
                                        
        echo '<br>3)<br>';
        
        foreach ($fruits as $i) {
            if ($i <> 'банан - это фрукт') {
            echo $i.', ';
            }
        }
        ?>
    </body>
</html>
