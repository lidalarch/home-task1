<?php

class Person {

    private $time = 0;
    private $money = 0;

    public function __construct(int $time, int $money) {
        if (($time >= 0) && ($money >= 0)) {
            $this->time = $time;
            $this->money = $money;
        }
    }

    public function setResources(int $time, int $money) {
        if (($time >= 0) && ($money >= 0)) {
            $this->time = $time;
            $this->money = $money;
        }
    }

    public function go() {
        $taxi = new Taxi();
        $metro = new Metro();
        $feet = new Feet();

        echo "<br>У меня есть {$this->time}мин времени, {$this->money}руб денег.<br>";

        if (($this->time >= $taxi->eatsTime) && ($this->money >= $taxi->eatsMoney)) {
            $taxi->move();
            $this->time -= $taxi->eatsTime;
            $this->money -= $taxi->eatsMoney;
            echo "Осталось {$this->time}мин времени, {$this->money}руб денег.<br>";
        } elseif (($this->time >= $metro->eatsTime) && ($this->money >= $metro->eatsMoney)) {
            $metro->move();
            $this->time -= $metro->eatsTime;
            $this->money -= $metro->eatsMoney;
            echo "Осталось {$this->time}мин времени, {$this->money}руб денег.<br>";
        } elseif (($this->time >= $feet->eatsTime) && ($this->money >= $feet->eatsMoney)) {
            $feet->move();
            $this->time -= $feet->eatsTime;
            $this->money -= $feet->eatsMoney;
            echo "Осталось {$this->time}мин времени, {$this->money}руб денег.<br>";
        } else {
            echo "Я опоздаю!<br>";
        }
    }

}

interface Transport {

    public function move();
}

class Taxi implements Transport {

    public $eatsTime = 5;
    public $eatsMoney = 200;

    public function move() {
        echo "Еду на такси.<br>";
    }

}

class Metro implements Transport {

    public $eatsTime = 10;
    public $eatsMoney = 20;

    public function move() {
        echo "Еду на метро.<br>";
    }

}

class Feet implements Transport {

    public $eatsTime = 30;
    public $eatsMoney = 0;

    public function move() {
        echo "Иду пешком.<br>";
    }

}

$me = new Person(40, 500);
$me->go();

$me->setResources(15, 30);
$me->go();

$me->setResources(40, 5);
$me->go();

$me->setResources(5, 5);
$me->go();
