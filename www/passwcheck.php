<?php
// Include necessary files
require_once '../sys/core/init.php';

// Set up the page title and CSS files
$page_title = 'Проверка пароля';
$css_files = array('style.css',);
$script_files = array();

// Output the header
require_once 'assets/common/header.php';
?>

<div>
    <form method="post" action="assets/inc/form-handler.php">

        <div id="checkpass"><?= $_SESSION['checkpass'] ?></div>

        <input type="hidden" name="token" value="<?= $_SESSION['token']; ?>" />
        <input type="hidden" name="action" value="user_signup_logout" />

        <button type="submit">На страницу регистрации</button>
    </form>
</div>
<?php
// Output the footer
require_once 'assets/common/footer.php';
?>