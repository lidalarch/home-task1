<?php
// Include necessary files
require_once '../sys/core/init.php';

// Set up the page title and CSS files
$page_title = "Форма регистрации";
$css_files = array('style.css',);
$script_files = array('jquery-3.4.1.min.js', 'signup-validate.js',);

// Output the header
require_once 'assets/common/header.php';
?>

<form id="reg_form" method="post" action="assets/inc/form-handler.php">
    <h1>Форма регистрации</h1>
    <div>
        <label for="log">Логин</label>
        <input type="text" name="login" id="log" placeholder="Username" />
        <div class="msg-box">
        </div>
    </div>
    <div>
        <label for="pass">Пароль</label>
        <input type="password" name="password" id="pass" placeholder="Password" />
        <div class="msg-box">
        </div>
    </div>

    <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
    <input type="hidden" name="action" value="user_signup" />

    <div>
        <button type="submit" id="register">Зарегистрироваться</button>
    </div>
</form>
<?php
// Output the footer
require_once 'assets/common/footer.php';
?>