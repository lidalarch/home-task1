<?php
// Include necessary files
require_once '../sys/core/init.php';

// Set up the page title and CSS files
$page_title = "Форма авторизации";
$css_files = array('style.css',);
$script_files = array();

// Output the header
require_once 'assets/common/header.php';
?>

<form method="post" action="assets/inc/form-handler.php">

    <div>
        <label for="log">Логин</label>
        <input type="text" name="login" id="log" placeholder="Username" />
    </div>
    <div>
        <label for="pass">Пароль</label>
        <input type="password" name="password" id="pass" placeholder="Password" />   
    </div>

    <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
    <input type="hidden" name="action" value="user_login" />

    <div>
        <button type="submit">Войти</button>
    </div>
    <div class="link">
        <a href="sign-up.php">Форма регистрации</a>
    </div>
</form>
<?php
// Output the footer
require_once 'assets/common/footer.php';
?>