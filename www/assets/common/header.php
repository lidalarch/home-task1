﻿<!DOCTYPE html>

<html lang="ru">

    <head>
        <meta charset="utf-8" />

        <title>
            <?php echo $page_title; ?>
        </title>

        <?php foreach ($css_files as $css): ?>

            <link rel="stylesheet" type="text/css" media="screen,projection"
                  href="assets/css/<?php echo $css; ?>" />

        <?php endforeach; ?>

        <?php foreach ($script_files as $script): ?>

            <script src="assets/js/<?php echo $script; ?>"></script>

        <?php endforeach; ?>

    </head>

    <body>
        <header>
            <h1></h1>
        </header>