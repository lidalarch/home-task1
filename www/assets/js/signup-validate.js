$(document).ready(function () {

    // обработчик потери фокуса для всех полей ввода
    $('input#log, input#pass').unbind().blur(function () {

        // Записываем обращения к атрибуту и значению каждого поля в переменные
        var id = $(this).attr('id');
        var val = $(this).val();

        // После того, как поле потеряло фокус, перебираем значения id, совпадающие с id данного поля
        switch (id)
        {
            case 'log':
                var rv_log = /^[a-zA-Z0-9]+$/;

                // Eсли длина имени > 2симв, оно не пустое и уд. regex,
                // то доб. этому полю класс right
                // и ниже в div для сообщений выводим, что знач-е валидно

                if (val.length > 2 && val != '' && rv_log.test(val))
                {
                    $(this).removeClass('wrong').addClass('right');
                    $(this).next('.msg-box').text('Принято')
                            .css('color', 'green')
                            .animate({'paddingLeft': '10px'}, 400)
                            .animate({'paddingLeft': '5px'}, 400);
                }

                // Иначе: удаляем класс right, добавляем wrong
                // и ниже выводим сообщение об ошибке и параметры для верной валидации

                else
                {
                    $(this).removeClass('right').addClass('wrong');
                    $(this).next('.msg-box').html('&bull; поле "Логин" обязательно для заполнения<br>&bull; длина должна составлять не менее 2 символов<br>&bull; поле должно содержать только латинские буквы или цифры')
                            .css('color', 'red')
                            .animate({'paddingLeft': '10px'}, 400)
                            .animate({'paddingLeft': '5px'}, 400);
                }
                break;

                // Валидация пароля: от 8 символов, содержит знаки, заглавные, строчные, цифры
            case 'pass':
                var rv1_pass = /[A-Z]/;
                var rv2_pass = /[a-z]/;
                var rv3_pass = /[0-9]/;
                var rv4_pass = /[-!"^#$%&'()*+,./:;<=>?@\[\\\]_`{|}~]/;
                if (val.length > 7 && val != '' && rv1_pass.test(val) && rv2_pass.test(val) && rv3_pass.test(val) && rv4_pass.test(val) )
                {
                    $(this).removeClass('wrong').addClass('right');
                    $(this).next('.msg-box').text('Принято')
                            .css('color', 'green')
                            .animate({'paddingLeft': '10px'}, 400)
                            .animate({'paddingLeft': '5px'}, 400);
                } else
                {
                    $(this).removeClass('right').addClass('wrong');
                    $(this).next('.msg-box').html('&bull; поле "Пароль" обязательно для заполнения<br>&bull; длина должна составлять не менее 8 символов<br>&bull; поле должно содержать заглавные и строчные латинские буквы, цифры и знаки препинания<br>')
                            .css('color', 'red')
                            .animate({'paddingLeft': '10px'}, 400)
                            .animate({'paddingLeft': '5px'}, 400);
                }
                break;

        } // end switch

    }); // end blur

        // отправляем данные формы
    $('button#register').click(function (e) {

        // Запрещаем стандартное поведение для кнопки submit
        e.preventDefault();

        // Проверка по нажатию кнопки "Зарегистрироваться":
        // если кол-во полей с классом right = 2 => все поля заполнены верно =>
        // отправляем форму в assets/inc/form-handler.php
        
        if ($('.right').length == 2)
        {
            $('form#reg_form').submit();
            $('input#log, input#pass').val('').removeClass().next('.msg-box').text('');
            
        } else {
            return false;

        }
        
    }); // end submit

}); // end script