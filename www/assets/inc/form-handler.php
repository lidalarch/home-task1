<?php

// Include necessary files
require_once __DIR__ . '/../../../sys/core/init.php';

// Create a lookup array for form actions
$actions = array(
	'user_login' => array(
		'object' => 'LoginController',
		'method' => 'processLoginForm',
		'header' => '../../success.php'
		),

	'user_logout' => array(
		'object' => 'LoginController',
		'method' => 'processLogout',
		'header' => '../../'
		),
    
    	'user_signup' => array(
		'object' => 'LoginController',
		'method' => 'processRegister',
		'header' => '../../passwcheck.php'
		),
    
        'user_signup_logout' => array(
		'object' => 'LoginController',
		'method' => 'processLogout',
		'header' => '../../sign-up.php'
		),

	);

// Make sure the anti-CSRF token was passed and that the requested action exists in the lookup array

if ( ($_POST['token'] == $_SESSION['token']) && isset($actions[$_POST['action']]) ) {

    $use_array = $actions[$_POST['action']];
    $obj = new $use_array['object'];
    $MethodName = $use_array['method'];
    $metodResult = $obj->$MethodName();

    if ($metodResult === true) {

        header("Location: " . $use_array['header']);
        exit;

    } else {			// If an error occured, output it and end execution

        $css_files = array('style.css', );
        $script_files = array();
        include_once '../common/header.php';
        die ( "ERROR: " . $metodResult );
    }
} else {			// Redirect to the main index if the token/action is invalid

    header("Location: ../../");
    exit;
}