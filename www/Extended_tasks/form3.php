<?php

session_start();
$_SESSION['pages'][] = $_SERVER['PHP_SELF'];

if (isset($_POST['YourSurname'])) {
	
	$YourSurname = stripslashes (strip_tags (trim (htmlspecialchars ($_POST['YourSurname'],ENT_QUOTES))));
	$_SESSION['user']['surname'] = $YourSurname;
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Задание сессии</title>
    </head>
    
    <body>
        <form method="post" action="page4.php">
            
            <div>
                <label for="YourBirth">Введите дату рождения</label>
                <input type="date" name="YourBirth" id="YourBirth" />
            </div>

            <div>
                <button type="submit">Дальше</button>
            </div>
        </form>
    </body>
</html>
