<?php

session_start();
$_SESSION = [];

$_SESSION['pages'][] = $_SERVER['PHP_SELF'];
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Задание сессии</title>
    </head>
    
    <body>
        <form method="post" action="form2.php">
            
            <div>
                <label for="YourName">Введите имя</label>
                <input type="text" name="YourName" id="YourName" placeholder="Имя" />
            </div>

            <div>
                <button type="submit">Дальше</button>
            </div>
        </form>
    </body>
</html>
