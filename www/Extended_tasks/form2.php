<?php

session_start();
$_SESSION['pages'][] = $_SERVER['PHP_SELF'];

if (isset($_POST['YourName'])) {
	
	$YourName = stripslashes (strip_tags (trim (htmlspecialchars ($_POST['YourName'],ENT_QUOTES))));
	$_SESSION['user']['name'] = $YourName;
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Задание сессии</title>
    </head>
    
    <body>
        <form method="post" action="form3.php">
            
            <div>
                <label for="YourSurname">Введите фамилию</label>
                <input type="text" name="YourSurname" id="YourSurname" placeholder="Фамилия" />
            </div>

            <div>
                <button type="submit">Дальше</button>
            </div>
        </form>
    </body>
</html>
