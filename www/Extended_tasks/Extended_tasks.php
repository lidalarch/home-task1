<?php

$logCount = 0;
if (isset ($_COOKIE['logCount']) && is_numeric($_COOKIE['logCount'])){
	$logCount = $_COOKIE['logCount']*1;
}
$logCount++;

$lastVisit = '';
if (isset ($_COOKIE['lastVisit'])){
	$lastVisit = htmlspecialchars($_COOKIE['lastVisit'],ENT_QUOTES);
	$lastVisit = stripslashes(trim($lastVisit));
}

date_default_timezone_set ('ASIA/Novosibirsk');

setcookie('logCount',$logCount,0x7FFFFFFF);
setcookie('lastVisit',date('Y-m-d H:i:s T'),0x7FFFFFFF);

if ($logCount == 1) {
	echo '<h3>Добро пожаловать!</h3>';
} else {
	echo <<<HTML
	<h3>Вы здесь уже $logCount раз</h3>
	<p>Последнее посещение: $lastVisit</p>
HTML;
}

$name = 'Lida';
$age = 36;
echo "Меня зовут $name.<br />";
echo PHP_EOL;
print ("Меня зовут $name.<br />\n");
echo "Мне $age лет.<br />\n";
unset ($age);
const A = 'константа';
echo defined (A);
echo "\n".A."<br />\n";
//A = 'константа1'; - ошибка

echo "<br />\n";

$age = -5;
if (($age >= 18) && ($age <= 59)) {
	echo "Вам еще работать и работать.<br />\n";
} elseif ($age > 59) {
	echo "Вам пора на пенсию.<br />\n";
} elseif (($age >= 0) && ($age < 18)){
	echo "Вам еще рано работать.<br />\n";
} else {
	echo "Неизвестный возраст.<br />\n";
}

echo "<br />\n";

$day = 2;
switch ($day) {
	case 1:
	case 2:
	case 3:
	case 4:
	case 5: echo "Это рабочий день.<br />\n"; break;
	case 6:
	case 7: echo "Это выходной день.<br />\n"; break;
	default: echo "Неизвестный день.<br />\n";
}

echo "<br />\n";

$keys = ['model', 'speed', 'doors', 'year'];
$val_bmv = ['X5', '120', '5', '2006'];
$bmv = array_combine ($keys, $val_bmv);

$val_toy = ['Carina', '130', '4', '2007'];
$val_op = ['Corsa', '140', '5', '2007'];

$toyota = array_combine ($keys, $val_toy);
$opel = array_combine ($keys, $val_op);

echo "name - model - speed - doors - year<br />\n";
echo "BMV - {$bmv['model']} - {$bmv['speed']} - {$bmv['doors']} - {$bmv['year']}<br />\n";
echo "Toyota - {$toyota['model']} - {$toyota['speed']} - {$toyota['doors']} - {$toyota['year']}<br />\n";
echo "Opel - {$opel['model']} - {$opel['speed']} - {$opel['doors']} - {$opel['year']}<br />\n";

echo "<br />\n";

for ($i=1; $i<=50; $i+=2) {
	echo "$i<br />\n";
}

echo "<br />\n";

$cols = 5;
$rows = 4;
echo '<table style="border: 1px solid black">';
for ($i=1; $i<=$rows; $i++) {
	echo '<tr>';
	for ($j=1; $j<=$cols; $j++) {
		if (($i == 1)||($j == 1)) {	
		echo '<th style="background-color: lightblue;" >'.$i*$j.'</th>';
		} else {
			echo '<td>'.$i*$j.'</td>';
		}
	}
	echo '</tr>';
}
echo '</table>';

$menuCells = ['Home', 'About', 'Contacts', 'Catalog'];
$menuHref = ['index.php', 'about.php', 'contacts.php', 'catalog.php'];
$menu = array_combine ($menuCells, $menuHref);

echo '<menu>';
foreach ($menu as $cell => $href){
	echo "<li><a href=\"$href\">$cell</a></li>";
}
echo '</menu>';

