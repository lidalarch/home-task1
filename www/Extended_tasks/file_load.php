<?php

if ($_SERVER[REQUEST_METHOD] == 'POST') {
	if ($_FILES[userfile][error] == 0){
		
		$filePath = $_FILES[userfile][tmp_name];
		$fileName = $_FILES[userfile][name];
		
		$dirName = 'Upload';
		mkdir ($dirName);
		if (is_dir($dirName) && $_FILES[userfile][size]<1024){
			
			$fileNewPath = $dirName . '/' . $fileName;
			move_uploaded_file ($filePath, $fileNewPath);
		}
	}

}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Задание загрузка файлов</title>
    </head>
    
    <body>
		<h3>Загрузите файл</h3>
		<p>не более 1Мб</p>
		
        <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">
            
            <div>
                <label for="YourFile">Укажите путь к файлу</label>
                <input type="file" name="userfile" id="YourFile" />
            </div>

            <div>
                <button type="submit">Отправить</button>
            </div>
			
        </form>
    </body>
</html>
