<?php 
function calc_age($birthday) {
	
  $birthday_time = strtotime($birthday);
  $age = date('Y') - date('Y', $birthday_time);
  if (date('md', $birthday_time) > date('md')) {
    $age--;
  }
  return $age;
}


echo calc_age('1983-01-20');