<?php

const FNM = 'users';

if (isset($_POST['fname'], $_POST['lname']) && !empty($_POST['fname']) && !empty($_POST['lname'])) {
	
	$fname = stripslashes (strip_tags (trim (htmlspecialchars ($_POST['fname'],ENT_QUOTES))));
	$lname = stripslashes (strip_tags (trim (htmlspecialchars ($_POST['lname'],ENT_QUOTES))));
	
	date_default_timezone_set ('ASIA/Novosibirsk');
	$line = sprintf('Имя: %1s Фамилия: %2s Зарегистрирован %3s', $fname, $lname, date('Y-m-d H:i:s T').PHP_EOL);
	
	$fileRes = fopen(FNM,'ab');
    if (is_resource ($fileRes)) {
		fwrite ($fileRes, $line);
		fclose ($fileRes);
	}
	
	header('Location:' . $_SERVER['PHP_SELF']);
	exit();
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Задание файлы</title>
    </head>
    
    <body>
		<h3>Заполните форму</h3>
        <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>">
            
            <div>
                <label for="YourName">Введите имя</label>
                <input type="text" name="fname" id="YourName" placeholder="Имя" />
            </div>
            <div>
                <label for="YourSurname">Введите фамилию</label>
                <input type="text" name="lname" id="YourSurname" placeholder="Фамилия" />
            </div>

            <div>
                <button type="submit">Отправить</button>
            </div>
			
			<div>
			<?php
			
			if (file_exists (FNM)) {
				
				echo '<ol>';
				
				$file = file (FNM);
				foreach ($file as $ln) {
					
					echo "<li>$ln</li>";
				}
				echo '</ol>';
				
				echo '<p>Размер файла ' . FNM . ': ' . filesize(FNM) . ' байт.</p>';
			}

			?>
			</div>
        </form>
    </body>
</html>
