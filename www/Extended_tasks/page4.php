<?php

session_start();
$_SESSION['pages'][] = $_SERVER['PHP_SELF'];

function calc_age($birthday) {
	
  $birthday_time = strtotime($birthday);
  $age = date('Y') - date('Y', $birthday_time);
  if (date('md', $birthday_time) > date('md')) {
    $age--;
  }
  return $age;
}

if (isset($_POST['YourBirth'])) {
	
	$YourBirth = stripslashes (strip_tags (trim (htmlspecialchars ($_POST['YourBirth'],ENT_QUOTES))));
	$_SESSION['user']['birthday'] = $YourBirth;

	$YourAge = calc_age($_SESSION['user']['birthday']);
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Задание сессии</title>
    </head>
    
    <body>
            <div>
				<p>Ваше имя - <?= $_SESSION['user']['name'] ?>.</p>
				<p>Ваша фамилия - <?= $_SESSION['user']['surname'] ?>.</p>
				<p>Вам <?= $YourAge ?> лет.</p>
            </div>
			
            <div>
				<?php
					if (isset($_SESSION['pages'])) {
						
						echo '<p>Список посещенных страниц:</p>';
						echo '<ol>';
						foreach ($_SESSION['pages'] as $page) {
							echo "<li>$page</li>";
						}
						echo '</ol>';
					}
				?>
            </div>
    </body>
</html>
<?php 
    unset($_SESSION);
    session_destroy();
    exit();
?>