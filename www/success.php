<?php
// Include necessary files
require_once '../sys/core/init.php';

// If the user is not logged in, send them to the main file
if (!isset($_SESSION['user'])) {

    header("Location: index.php");
    exit;
}

$userRaw = $_SESSION['user'];

if ($userRaw instanceof User) {
    $user = $userRaw;
} else {
    header("Location: index.php");
    exit;
}

// Set up the page title and CSS files
$page_title = 'Личная страница ' . $user->getLogin();
$css_files = array('style.css',);
$script_files = array();

// Output the header
require_once 'assets/common/header.php';
?>

<div>
    <form method="post" action="assets/inc/form-handler.php">

        <h1>Привет, <?= $user->getLogin() ?>!</h1>
        <p>Вы вошли <?= (new LoginCounter())->countLines(loginlogfile) ?> раз.</p>
        <p><?= (new Usage())->maxUsage(loginlogfile) ?></p>
        <div id="weather"><?= (new Weather())->showWeather() ?></div>

        <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
        <input type="hidden" name="action" value="user_logout" />

        <button type="submit">Выйти</button>
    </form>
</div>
<?php
// Output the footer
require_once 'assets/common/footer.php';
?>