<?php

// Create an empty array to store constants
$Constants = array();

// The database server
$Constants['serverName'] = 'localhost';

$Constants['port'] = '3306';

// The name of the database to work with
$Constants['dbase'] = 'home_task1_db';

// The database username
$Constants['username'] = 'root';

// The database password
$Constants['password'] = '';

//Login log path & name
$Constants['loginlogfile'] = __DIR__ . '/../logs/login_log';
