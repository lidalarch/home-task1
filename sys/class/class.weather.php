<?php

class Weather {

    protected $apiURL = 'https://api.weatherbit.io/v2.0/current?key=8462689bb0404d509b2cada44d36afe5&lang=ru&city=Novosibirsk';
    protected $weather = [];
    public $formatWeather = '';

    protected function getWeather($apiURL) {
        $apiRequest = curl_init($apiURL);
        curl_setopt($apiRequest, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($apiRequest);
        $decodedResult = json_decode($result);
        $this->weather = $decodedResult->data[0];

        curl_close($apiRequest);

        return (empty($this->weather)) ? false : true;
    }

    protected function formatWeather() {
        if (empty($this->weather)) {
            return false;
        }
        $city = $this->weather->city_name;
        $temp = $this->weather->temp;
        $windSpeed = $this->weather->wind_spd;
        $windDir = $this->weather->wind_cdir_full;
        $windDir = mb_convert_case($windDir, MB_CASE_LOWER, 'UTF-8');
        $humidity = $this->weather->rh;
        $weatherDescription = $this->weather->weather->description;
        $weatherDescription = mb_convert_case($weatherDescription, MB_CASE_LOWER, 'UTF-8');

        $formatWeather = <<<HTML_MARKUP
<p>Погода в городе {$city}:</p>
<p>Температура {$temp}ºC, ветер {$windDir} {$windSpeed}м/с, влажность {$humidity}%, {$weatherDescription}.</p>
HTML_MARKUP;

        $this->formatWeather = $formatWeather;

        return (empty($this->formatWeather)) ? false : true;
    }

    public function showWeather() {
        $this->getWeather($this->apiURL);
        $this->formatWeather();

        return $this->formatWeather;
    }

}
