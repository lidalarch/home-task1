<?php

class Authenticator extends DB_Connect {

    public function __construct($db = NULL) {   // Sets db connection
        parent::__construct($db);
    }

    public function checkCredentials($user): bool {

        $uname = htmlentities($_POST['login'], ENT_QUOTES);  // Escapes the user input for security
        $pword = htmlentities($_POST['password'], ENT_QUOTES);

        $sql = "SELECT `LOGIN`,`PASSWORD` FROM `users` WHERE `LOGIN` = :log LIMIT 1";

        try {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':log', $uname, PDO::PARAM_STR);
            $stmt->execute();
            $userRaw = $stmt->fetch(PDO::FETCH_ASSOC);
            $username = $userRaw['LOGIN'];
            $hashStored = $userRaw['PASSWORD'];
            $stmt->closeCursor();
        } catch (Exception $e) {
            die($e->getMessage());
        }

        If ($uname == $username) {  // Checks username from DB
            $user->setLogin($uname);
            $user->setPassword($hashStored);
        } else {      // Fails if username doesn't match
            return false;
        }

        $hash = password_verify($pword, $user->getPassword());  // Get the hash of the user-supplied password

        if ($user->getPassword() == $hash) {  // Checks if the hashed password matches the stored hash
            return true;
        } else {         // Fails if the passwords don't match
            return false;
        }
    }

}
