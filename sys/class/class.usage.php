<?php

class Usage {

    public function maxUsage($file) {

        $lines = file($file, FILE_SKIP_EMPTY_LINES);
        $pattern = '/(?<=logged in on )(?:\d{4}-\d\d-\d\d (\d\d):\d\d:\d\d [+-]\d\d)/';
        $matches = [];

        foreach ($lines as $line) {
            preg_match($pattern, $line, $matches[]);
        }

        $SubMatches = array_column($matches, 1);
        arsort($SubMatches);
        $hours = array_count_values($SubMatches);

        $maxCount = max($hours);
        $maxHour = [];

        foreach ($hours as $hour => $count) {
            if ($hours[$hour] == $maxCount) {
                $maxHour[] = $hour * 1;
            }
        }

        $html = implode(', ', $maxHour);
        $format = 'Чаще всего посетители заходят на страницу в %s часов.';
        $htmlFormat = sprintf($format, $html);

        return $htmlFormat;
    }

}
