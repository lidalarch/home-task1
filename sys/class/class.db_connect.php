<?php

class DB_Connect {  // Establishes a database connection

    public $db;  // Stores a database object

    public function __construct($dbo = NULL) {  // Checks for a DB object or creates one if one isn't found ($dbo is a database object)
        if (is_object($dbo)) {
            $this->db = $dbo;
        } else {  // Constants are defined in /sys/config/db-conf.php
            $dsn = 'mysql:host=' . serverName . ';port=' . port . ';dbname=' . dbase . ';charset=utf8';
            try {
                $connection = new PDO($dsn, username, password);
                $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->db = $connection;
            } catch (Exception $e) {  // If the DB connection fails, output the error
                die($e->getMessage());
            }
        }
    }

}
