<?php

class LoginController {
        
    public function processLoginForm() {  // Checks login credentials for a valid user; returns TRUE on success, message on error
        if ($_POST['action'] != 'user_login') {  // Fails if the proper action was not submitted
            return "В processLoginForm передано недействительное значение атрибута action.";
        }

        $user = new User();
        $result = $this->getAuth()->checkCredentials($user);
        
        if ($result === true) {

            $_SESSION['user'] = $user;
            
            $this->addLoginLog(loginlogfile, $user);

            return true;
        } else {

            return "Неверное имя пользователя или пароль.";
        }
    }

    protected function addLoginLog($logFilePath, $user) {

        date_default_timezone_set('ASIA/Novosibirsk');
        $formatLogin = sprintf('%1s logged in on %2s', $user->getLogin(), date('Y-m-d H:i:s T' . PHP_EOL));

        $this->getLogger()->addlog($logFilePath, $formatLogin);
    }


    public function processLogout() {  // Logs out the user, returns TRUE on success or messsage on failure
        if ($_POST['action'] != ('user_logout' || 'user_signup_logout')) {  // Fails if the proper action was not submitted
            return "В processLogout передано недействительное значение атрибута action.";
        }

        unset($_SESSION['user']);  // Removes the user array from the current session
        session_destroy();

        return TRUE;
    }

    protected function getAuth() {
        return new Authenticator();
    }

    protected function getLogger() {
        return new Logger();
    }
    
    protected function getPCheck() {
        return new Passwcheck();
    }
    
    public function processRegister() { 
        if ($_POST['action'] != 'user_signup') {
            return "В processRegister передано недействительное значение атрибута action.";
        }
        
        $result = $this->getPCheck()->showValid();
        
        if (isset($result)) {

            $_SESSION['checkpass'] = $result;
            
        } else {

            return "Не удалось проверить пароль.";
        }


        return true;
    }

}
