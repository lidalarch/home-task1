<?php

class User
{
	protected $login = 'Unknown';
	protected $password = '';

	public function getLogin() {
		return $this->login;
	}

	public function getPassword() {  
		return $this->password;
	}

	public function setLogin($login) {

		if(is_string($login)) {
			$this->login = (string)$login;
		}
	}

	public function setPassword($password) {

		if(is_string($password)) {

			$this->password = (string)$password;
		}
	}
}