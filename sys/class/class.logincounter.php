<?php

class LoginCounter {

    public function countLines($file): int {

        $lines = count(file($file, FILE_SKIP_EMPTY_LINES));
        return $lines;
    }

}
