<?php

class Passwcheck {

    public function showValid() {

        $uname = htmlentities($_POST['login'], ENT_QUOTES);  // Escapes the user input for security
        $pword = htmlentities($_POST['password'], ENT_QUOTES);

        $pattern = '/^(?=.{8,}$)([[:graph:]]*(?:(?:[[:punct:]A-Z]+[a-z0-9]+)|(?:[a-z0-9]+[[:punct:]A-Z]+))[[:graph:]]*)$/'; //от 8 симв, сод. (знаки или заглавные)и(строчные или цифры)

        if (preg_match($pattern, $pword) == 1) {
            return "<p>Пароль сильный.</p>";
        } else {
            return "<p>Пароль слабый.</p>";
        }
    }

}
