<?php

// Enable sessions
session_start();

// Generate an anti-CSRF token if one doesn't exist
if ( !isset($_SESSION['token']) ) {

	$_SESSION['token'] = sha1(time());
}

// Include the necessary configuration info
include_once __DIR__.'/../config/db-conf.php';

// Define constants for configuration info
foreach ( $Constants as $name => $val ) {
	define($name, $val);
}

// Define the auto-load function for classes
function __autoload($class) {

	$filename =  __DIR__."/../class/class." . strtolower($class) . ".php";
	if ( file_exists($filename) ) {

		include_once $filename;
	}
}